WHID Injector was born from the need for cheap and dedicated hardware that could be remotely controlled in order to conduct HID attacks. WHID stands for WiFi HID Injector. It is a cheap but reliable piece of hardware designed to fulfill pentesters needs related to HID Attacks, during their engagements. The core of WHID is mainly an Atmega 32u4 (commonly used in many Arduino boards) and an ESP-12s (which provides the WiFi capabilities and is commonly used in IoT projects).

* What you can do with WHID Mobile Connector?

- Easily and Quickly Control and Inject Keystrokes Remotely over WiFi.
- Create New Payloads and Store Locally on your Phone. (Thus you won't need to upload them on the WHID Injector's memory and thus make Blue Teams life a bit harder in case of a Forensics Analysis.)
- Modify Existing Payloads. (Yes. It is already delivered with a set of cool payloads in it!)
- Remotely Configure WHID Injector (e.g. WiFi and IP Settings, OTA Firmware Update, Format FileSystem, AutoPayload feature, etc.).

Wanna know more about WHID Injector and the Mobile Connector, but too lazy to read a wiki?
Here a Video Presentation of WHID Injector: https://www.youtube.com/watch?v=ADqMCKtufNY
And here the WHID Mobile Connector: https://www.youtube.com/watch?v=7xf3JnbyIho

* Thus you won't need to upload them on the WHID Injector's memory and thus make Blue Teams life a bit harder in case of a Forensics Analysis.

* Follow us on Twitter at https://twitter.com/whid_injector
WHID Injector's repo on GitHub https://github.com/whid-injector/WHID

* WHID Mobile Connector is OpenSource! Wanna contribute? Look at https://github.com/whid-injector/WHID-Mobile-Connector

This app is built and signed by Kali NetHunter.
 
